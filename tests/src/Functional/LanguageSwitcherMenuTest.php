<?php

namespace Drupal\Tests\language_switcher_menu\Functional;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Provides functional tests for Language Switcher Menu module.
 *
 * @group language_switcher_menu
 */
class LanguageSwitcherMenuTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'language',
    'language_switcher_menu',
    'language_test',
    'locale',
    'locale_test',
    'menu_ui',
    'menu_test',
    'node',
    'test_language_switcher_menu',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Users created during set-up.
   *
   * @var \Drupal\user\Entity\User[]
   */
  protected $users;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create users.
    $this->users['admin_user'] = $this->drupalCreateUser([
      'administer blocks',
      'administer languages',
      'access administration pages',
      'configure language_switcher_menu',
      'view language_switcher_menu links',
    ]);
    $this->users['admin_site_config'] = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->users['admin_menu_any'] = $this->drupalCreateUser([
      'administer menu',
      'link to any page',
    ]);
    $this->users['admin_menu_regular'] = $this->drupalCreateUser([
      'administer menu',
      'view language_switcher_menu links',
    ]);
    $this->users['access_content'] = $this->drupalCreateUser([
      'access content',
    ]);
    $this->users['view_links'] = $this->drupalCreateUser([
      'view language_switcher_menu links',
    ]);

    // Create a node type and make it translatable.
    \Drupal::entityTypeManager()->getStorage('node_type')
      ->create([
        'type' => 'page',
        'name' => 'Page',
      ])
      ->save();

    // Create a published node.
    $node = \Drupal::entityTypeManager()->getStorage('node')
      ->create([
        'type' => 'page',
        'title' => $this->randomMachineName(),
        'status' => 1,
      ]);
    $node->save();

    // Initialize state for test language switcher module.
    \Drupal::state()->set('test_language_switcher_menu.remove_current_language_switch_link', FALSE);
  }

  /**
   * Tests language switch links provided by Language Switcher Menu module.
   */
  public function testLanguageSwitchLinks(): void {
    $this->drupalLogin($this->users['admin_user']);

    // Add a language.
    $edit = [
      'predefined_langcode' => 'fr',
    ];
    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm($edit, 'Add language');

    // Enable URL language detection and selection.
    $edit = ['language_interface[enabled][language-url]' => '1'];
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm($edit, 'Save settings');

    // Place menu block.
    $this->drupalPlaceBlock('system_menu_block:main');

    // Configure module.
    $edit = [
      'type' => 'language_interface',
      'parent' => 'main:',
      'weight' => '1',
    ];
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertMenuLinks([
      'Home' => '/',
      'English' => '/admin/config/regional/language_switcher_menu',
      'French' => '/fr/admin/config/regional/language_switcher_menu',
    ], 'The language links have been added to the menu after "Home".');

    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertMenuLinks([
      'Home' => '/',
      'English' => '/user/' . $this->users['admin_user']->id(),
      'French' => '/fr/user/' . $this->users['admin_user']->id(),
    ], 'The language links have been added to the menu after "Home".');

    // Reconfigure weight.
    $edit = [
      'type' => 'language_interface',
      'parent' => 'main:',
      'weight' => '-10',
    ];
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertMenuLinks([
      'English' => '/admin/config/regional/language_switcher_menu',
      'French' => '/fr/admin/config/regional/language_switcher_menu',
      'Home' => '/',
    ], 'The language links have been added to the menu before "Home".');

    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertMenuLinks([
      'English' => '/user/' . $this->users['admin_user']->id(),
      'French' => '/fr/user/' . $this->users['admin_user']->id(),
      'Home' => '/',
    ], 'The language links have been added to the menu before "Home".');

    // Check access for anonymous users.
    $this->drupalLogout();
    $this->assertMenuLinks([
      'Home' => '/',
    ], 'The language links are not visible to anonymous users.');
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(403);

    // Check access for authenticated users.
    $this->drupalLogin($this->users['access_content']);
    $this->assertMenuLinks([
      'Home' => '/',
    ], 'The language links are not visible for authenticated users without permission to view them.');
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->users['view_links']);
    $this->drupalGet('/user/' . $this->users['view_links']->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertMenuLinks([
      'English' => '/user/' . $this->users['view_links']->id(),
      'French' => '/fr/user/' . $this->users['view_links']->id(),
      'Home' => '/',
    ], 'The language links are visible for authenticated users with permission to view them.');
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(403);

    // Check that links are visible on pages that deny access.
    $this->drupalGet('user/' . $this->users['admin_user']->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertMenuLinks([
      'English' => '/system/403',
      'French' => '/fr/system/403',
      'Home' => '/',
    ], 'The language links are visible on pages that deny access.');

    // Check that links are shown in a non-standard language.
    $this->clickLink('French');
    $this->assertMenuLinks([
      'English' => '/system/403',
      'French' => '/fr/system/403',
      'Home' => '/fr',
    ], 'The menu links are correct in a non-standard language.');

    // Assert that main menu management page doesn't show a white page.
    $this->drupalLogin($this->users['admin_menu_regular']);
    $this->drupalGet('admin/structure/menu/manage/main');
    $this->assertSession()->statusCodeEquals(200);

    // Assert that menu switch links are visible.
    $this->assertMenuLinks([
      'English' => '/admin/structure/menu/manage/main',
      'French' => '/fr/admin/structure/menu/manage/main',
      'Home' => '/',
    ], 'Both language links are shown.');

    // Enable removal of current language switch link in test module.
    \Drupal::state()->set('test_language_switcher_menu.remove_current_language_switch_link', TRUE);

    // Assert that main menu management page still doesn't show a white page.
    $this->drupalGet('admin/structure/menu/manage/main');
    $this->assertSession()->statusCodeEquals(200);

    // Assert that menu switch link in current language has been removed.
    $this->assertMenuLinks([
      'French' => '/fr/admin/structure/menu/manage/main',
      'Home' => '/',
    ], 'The language link for the current language is not shown.');

    // Disable removal of current language switch link in test module.
    \Drupal::state()->set('test_language_switcher_menu.remove_current_language_switch_link', FALSE);

    // Assert that menu switch links are visible.
    $this->drupalGet('admin/structure/menu/manage/main');
    $this->assertMenuLinks([
      'English' => '/admin/structure/menu/manage/main',
      'French' => '/fr/admin/structure/menu/manage/main',
      'Home' => '/',
    ], 'Both language links are visible in the menu.');

    // Change the front page to /node/1.
    $this->drupalLogin($this->users['admin_site_config']);
    $edit = ['site_frontpage' => '/node/1'];
    $this->drupalGet('admin/config/system/site-information');
    $this->submitForm($edit, 'Save configuration');

    // Assert that menu switch links link to /.
    $this->drupalLogin($this->users['view_links']);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertMenuLinks([
      'English' => '/',
      'French' => '/fr',
      'Home' => '/',
    ], 'Language switch links do not link to node path on front page.');

    // Reconfigure to disable.
    $this->drupalLogin($this->users['admin_user']);
    $edit = [
      'type' => 'language_interface',
      'parent' => '',
      'weight' => '1',
    ];
    $this->drupalGet('admin/config/regional/language_switcher_menu');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertMenuLinks([
      'Home' => '/',
    ], 'The language links have been disabled.');

    // Check that a custom link targeting "<current>" (i.e. a link not added by
    // language_switcher_menu module) is not visible in menu.
    // @todo Remove once https://www.drupal.org/project/drupal/issues/3008889
    // has been fixed.
    $edit = [
      'link[0][uri]' => 'route:<current>',
      'title[0][value]' => 'link_current',
      'description[0][value]' => '',
      'enabled[value]' => 1,
      'expanded[value]' => 0,
      'menu_parent' => 'main:',
      'weight[0][value]' => 0,
    ];

    $this->drupalLogin($this->users['admin_menu_regular']);
    $this->drupalGet('admin/structure/menu/manage/main/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Error message');
    $this->assertSession()->pageTextContains("The path '<current>' is inaccessible.");
    $this->assertSession()->pageTextNotContains('The menu link has been saved.');
    $this->assertMenuLinks([
      'Home' => '/',
    ], 'Custom link targeting <current> is not visible in menu.');

    $this->drupalLogin($this->users['admin_menu_any']);
    $this->drupalGet('admin/structure/menu/manage/main/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Error message');
    $this->assertSession()->pageTextNotContains("The path '<current>' is inaccessible.");
    $this->assertSession()->pageTextContains('The menu link has been saved.');
    DeprecationHelper::backwardsCompatibleCall(
      currentVersion: \Drupal::VERSION,
      deprecatedVersion: '10.3',
      currentCallable: fn() => $this->assertMenuLinks([
        'Home' => '/',
      ], 'Custom link targeting <current> is not visible in menu with permission to link to any page.'),
      deprecatedCallable: fn() => $this->assertMenuLinks([
        'Home' => '/',
        'link_current' => '/admin/structure/menu/item/1/edit',
      ], 'Custom link targeting <current> is visible in menu with permission to link to any page.'),
    );

    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
    DeprecationHelper::backwardsCompatibleCall(
      currentVersion: \Drupal::VERSION,
      deprecatedVersion: '10.3',
      currentCallable: fn() => $this->assertMenuLinks([
        'Home' => '/',
      ], 'Custom link targeting <current> is not visible in menu with permission to link to any page.'),
      deprecatedCallable: fn() => $this->assertMenuLinks([
        'Home' => '/',
        'link_current' => '/node/1',
      ], 'Custom link targeting <current> is visible in menu with permission to link to any page.'),
    );

    $this->drupalLogin($this->users['view_links']);
    $this->assertMenuLinks([
      'Home' => '/',
    ], 'Custom link targeting <current> is not visible in menu.');
  }

  /**
   * Asserts that menu link labels and urls are shown as expected.
   *
   * @param string[] $expected
   *   Expected menu link URLs keyed by expected link label in expected order.
   * @param string $text
   *   Assert message.
   */
  protected function assertMenuLinks(array $expected, string $text): void {
    $language_switchers = $this->xpath('//nav/ul/li');
    $labels = [];
    $urls = [];
    foreach ($language_switchers as $list_item) {
      $link = $list_item->find('xpath', 'a');
      $urls[] = $link->getAttribute('href');
      $labels[] = $link->getText();
    }
    // Tests may be executed in a subdirectory (e.g. by Drupal CI). Run URLs
    // through \Drupal\Core\Url to get the correct URL in use.
    $expected_compare = [];
    foreach ($expected as $label => $url) {
      $url = Url::fromUserInput($url);
      $options = $url->getOptions();
      if (isset($options['query']['destination'])) {
        $options['query']['destination'] = Url::fromUserInput($options['query']['destination'])->toString();
      }
      $url->setOptions($options);
      $url = $url->toString();
      $expected_compare[$label] = $url;
    }
    $this->assertSame(array_keys($expected_compare), $labels, $text);
    $this->assertSame(array_values($expected_compare), $urls, $text);
  }

}
